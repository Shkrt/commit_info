class FetchCommitsJob < ActiveJob::Base
  queue_as :default

  def perform(args)
    commits = GithubAccess.new.get_commits(args[:user], args[:repo])
    CreateRecords.perform(commits) if commits
  end

  before_enqueue do |_job|
    Commit.delete_all if Commit.any?
    User.delete_all if User.any?
  end

  after_perform do |_j|
    sd = ScaleDrone.new(channel_id: ENV['SCALEDRONE_CHANNEL_ID'],
                        secret_key: ENV['SCALEDRONE_SECRET_KEY'])
    sd.publish('notifications', msg: I18n.t(:job_finished))
  end
end
