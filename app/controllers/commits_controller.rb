class CommitsController < ApplicationController
  def index
    commits = Commit.pages(params[:page], 20, 'created_at DESC')
    render locals: { commits: commits, page: params[:page] }
  end

  def search
    commits = User.find_commits(params[:search][:email], params[:page], 20, 'created_at DESC')
    render locals: { commits: commits, page: params[:page] }
  end

  def edit
    render locals: { commit: Commit.find(params[:id]), page: params[:page] }
  end

  def update
    Commit.find(params[:id]).update(commit_params)
    render :index, locals: { commits: Commit.pages(params[:page], 20, 'created_at DESC'), page: params[:page] }
  end

  private

  def commit_params
    params.require(:commit).permit(:username, :page)
  end
end
