class GithubController < ApplicationController
  def fetch
    FetchCommitsJob.perform_later api_params
    redirect_to root_path, notice: t(:job_enqueued)
  end

  private

  def api_params
    params.require(:repo_path).permit(:user, :repo)
  end
end
