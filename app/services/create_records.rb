class CreateRecords
  def self.perform(commits)
    users = []
    commits.each do |c|
      Commit.create(username: c.commit.committer.name, date: c.commit.committer.date,
                    sha: c.sha, description: c.commit.message)
      users |= [{ email: c.commit.committer.email, name: c.commit.committer.name }]
    end

    users.each { |u| User.create(name: u[:name], email: u[:email]) }
  end
end
