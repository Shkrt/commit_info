class GithubAccess
  attr_reader :github

  def initialize
    @github = Github.new auto_pagination: true, oauth_token: ENV['GITHUB_ACCESS_TOKEN']
  end

  def get_commits(user, repo)
    github.repos.commits.list(user: user, repo: repo)
  end
end
