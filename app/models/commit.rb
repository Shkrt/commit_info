class Commit < ActiveRecord::Base
  attr_accessor :page
  
  def self.pages(page_number, count, ordering)
    paginate(per_page: count, page: page_number).order(ordering)
  end
end
