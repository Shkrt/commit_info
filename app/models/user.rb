class User < ActiveRecord::Base
  validates :email, uniqueness: true

  def self.find_commits(email, page, count, order)
    user = find_by(email: email)
    user_commits = user.nil? ? Commit.none : Commit.where(username: user.name)
    user_commits.pages(page, count, order)
  end
end
