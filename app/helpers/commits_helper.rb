module CommitsHelper
  def pagination(commits)
    content_tag(:div, class: 'row col-md-4 col-md-offset-4') do
      js_will_paginate commits, params: { controller: 'commits', action: 'index', url: commits_path }
    end
  end

  def search_form(commits, page)
    if commits.any?
      render 'table', commits: commits, page: page
    else
      content_tag(:div, class:"col-md-12 h1") { t(:nothing_found) }
    end
  end

  def tr_selector(id)
    "commit_#{id}"
  end

  def form_selector(id)
    "commit_form_#{id}"
  end
end
