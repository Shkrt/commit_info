[![Code Climate](https://codeclimate.com/github/Shkrt/commit_info/badges/gpa.svg)](https://codeclimate.com/github/Shkrt/commit_info)
[![Test Coverage](https://codeclimate.com/github/Shkrt/commit_info/badges/coverage.svg)](https://codeclimate.com/github/Shkrt/commit_info/coverage)
[![Build Status](https://travis-ci.org/Shkrt/commit_info.svg?branch=master)](https://travis-ci.org/Shkrt/commit_info)
