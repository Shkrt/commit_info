class CreateCommits < ActiveRecord::Migration
  def change
    create_table :commits do |t|
      t.datetime :date, null: false
      t.string :sha, null: false
      t.string :description
      t.string :username

      t.timestamps null: false
    end
  end
end
