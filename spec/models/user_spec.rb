RSpec.describe User, type: :model do
  describe "self.find_commits" do
    let!(:user) { create :user_without_commits }
    let!(:user_a) { create :user_with_commits }

    before do
      19.times{create :commit}
    end

    context "when given user has no commits," do
      it "returns empty relation" do
        commits = User.find_commits(user.email, 1, 1, "created_at DESC")
        expect(commits.length).to be 0
      end
    end

    context "when given user has commits," do
      it "returns all commits that made by owner of given email, divide into pages" do
        commits_first_page = User.find_commits(user_a.email, 1, 10, "created_at DESC")
        commits_second_page = User.find_commits(user_a.email, 2, 10, "created_at DESC")
        expect(commits_first_page.length).to be 10
        expect(commits_second_page.length).to be 9
      end

      it "returns all commits ordered accoring to given order" do
        commits = User.find_commits(user_a.email, 1, 10, "created_at DESC")
        commits.each_cons(2) do |x, y|
          expect(x.date).to be > y.date
        end
      end
    end
  end
end
