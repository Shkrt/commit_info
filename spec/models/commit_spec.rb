RSpec.describe Commit, type: :model do
  describe 'self.pages' do
    before do
      19.times { create :commit }
    end

    it 'returns commits, divided into pages' do
      commits_first_page = Commit.pages(1, 10, 'created_at DESC')
      commits_second_page = Commit.pages(2, 10, 'created_at DESC')
      expect(commits_first_page.length).to be 10
      expect(commits_second_page.length).to be 9
    end

    it 'returns all commits ordered accoring to given order' do
      commits = Commit.pages(1, 10, 'created_at DESC')
      commits.each_cons(2) do |x, y|
        expect(x.date).to be > y.date
      end
    end
  end
end
