module GithubStubHelper
  def github_json
    [
      {
        "sha": '8702c22d1e2e0c2cb8b7fb9cdc33e90edd25cb8c',
        "commit": {
          "author": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T08:00:30Z'
          },
          "committer": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T08:00:30Z'
          },
          "message": 'improvements',
          "tree": {
            "sha": '157122dc8666b668dda83dbfc8641f5b41b28556',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/trees/157122dc8666b668dda83dbfc8641f5b41b28556'
          },
          "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/commits/8702c22d1e2e0c2cb8b7fb9cdc33e90edd25cb8c',
          "comment_count": 0
        },
        "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/8702c22d1e2e0c2cb8b7fb9cdc33e90edd25cb8c',
        "html_url": 'https://github.com/MockOfUser/sktchpd/commit/8702c22d1e2e0c2cb8b7fb9cdc33e90edd25cb8c',
        "comments_url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/8702c22d1e2e0c2cb8b7fb9cdc33e90edd25cb8c/comments',
        "author": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "committer": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "parents": [
          {
            "sha": 'f57b4f5985961c01645d0c6f5adb0f05f57c579b',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/f57b4f5985961c01645d0c6f5adb0f05f57c579b',
            "html_url": 'https://github.com/MockOfUser/sktchpd/commit/f57b4f5985961c01645d0c6f5adb0f05f57c579b'
          }
        ]
      },
      {
        "sha": 'f57b4f5985961c01645d0c6f5adb0f05f57c579b',
        "commit": {
          "author": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T07:52:41Z'
          },
          "committer": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T07:52:41Z'
          },
          "message": 'random colors added',
          "tree": {
            "sha": 'b4439f7be9c44aac4c30457671626d91f697d771',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/trees/b4439f7be9c44aac4c30457671626d91f697d771'
          },
          "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/commits/f57b4f5985961c01645d0c6f5adb0f05f57c579b',
          "comment_count": 0
        },
        "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/f57b4f5985961c01645d0c6f5adb0f05f57c579b',
        "html_url": 'https://github.com/MockOfUser/sktchpd/commit/f57b4f5985961c01645d0c6f5adb0f05f57c579b',
        "comments_url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/f57b4f5985961c01645d0c6f5adb0f05f57c579b/comments',
        "author": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "committer": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "parents": [
          {
            "sha": '17d50e48d0e6cfb98741579ebbf3aadc5670bb5a',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/17d50e48d0e6cfb98741579ebbf3aadc5670bb5a',
            "html_url": 'https://github.com/MockOfUser/sktchpd/commit/17d50e48d0e6cfb98741579ebbf3aadc5670bb5a'
          }
        ]
      },
      {
        "sha": '17d50e48d0e6cfb98741579ebbf3aadc5670bb5a',
        "commit": {
          "author": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T07:24:11Z'
          },
          "committer": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T07:24:11Z'
          },
          "message": 'style',
          "tree": {
            "sha": '44be546e93cd34888ca8949ec7a06360a75ba7d4',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/trees/44be546e93cd34888ca8949ec7a06360a75ba7d4'
          },
          "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/commits/17d50e48d0e6cfb98741579ebbf3aadc5670bb5a',
          "comment_count": 0
        },
        "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/17d50e48d0e6cfb98741579ebbf3aadc5670bb5a',
        "html_url": 'https://github.com/MockOfUser/sktchpd/commit/17d50e48d0e6cfb98741579ebbf3aadc5670bb5a',
        "comments_url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/17d50e48d0e6cfb98741579ebbf3aadc5670bb5a/comments',
        "author": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "committer": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "parents": [
          {
            "sha": '3fd223ab078f4632b3b7bdea65e08b819214b50c',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/3fd223ab078f4632b3b7bdea65e08b819214b50c',
            "html_url": 'https://github.com/MockOfUser/sktchpd/commit/3fd223ab078f4632b3b7bdea65e08b819214b50c'
          }
        ]
      },
      {
        "sha": '3fd223ab078f4632b3b7bdea65e08b819214b50c',
        "commit": {
          "author": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T06:44:02Z'
          },
          "committer": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-21T06:44:02Z'
          },
          "message": 'grid',
          "tree": {
            "sha": '2ba9e2de97dc3a6f9566a2cbb9be0c0067112d48',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/trees/2ba9e2de97dc3a6f9566a2cbb9be0c0067112d48'
          },
          "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/commits/3fd223ab078f4632b3b7bdea65e08b819214b50c',
          "comment_count": 0
        },
        "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/3fd223ab078f4632b3b7bdea65e08b819214b50c',
        "html_url": 'https://github.com/MockOfUser/sktchpd/commit/3fd223ab078f4632b3b7bdea65e08b819214b50c',
        "comments_url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/3fd223ab078f4632b3b7bdea65e08b819214b50c/comments',
        "author": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "committer": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "parents": [
          {
            "sha": '3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f',
            "html_url": 'https://github.com/MockOfUser/sktchpd/commit/3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f'
          }
        ]
      },
      {
        "sha": '3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f',
        "commit": {
          "author": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-20T19:37:19Z'
          },
          "committer": {
            "name": 'MockOfUser',
            "email": 'mail@mail.com',
            "date": '2015-04-20T19:37:19Z'
          },
          "message": 'initial commit',
          "tree": {
            "sha": '99d4d85d2b8ea22799a64b06dbee67b961dbd7df',
            "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/trees/99d4d85d2b8ea22799a64b06dbee67b961dbd7df'
          },
          "url": 'https://api.github.com/repos/MockOfUser/sktchpd/git/commits/3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f',
          "comment_count": 0
        },
        "url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f',
        "html_url": 'https://github.com/MockOfUser/sktchpd/commit/3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f',
        "comments_url": 'https://api.github.com/repos/MockOfUser/sktchpd/commits/3bf0080e7483f2a84f05b7c75b4da0c4f9ac056f/comments',
        "author": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "committer": {
          "login": 'MockOfUser',
          "id": 10_468_452,
          "avatar_url": 'https://avatars.githubusercontent.com/u/10468452?v=3',
          "gravatar_id": '',
          "url": 'https://api.github.com/users/MockOfUser',
          "html_url": 'https://github.com/MockOfUser',
          "followers_url": 'https://api.github.com/users/MockOfUser/followers',
          "following_url": 'https://api.github.com/users/MockOfUser/following{/other_user}',
          "gists_url": 'https://api.github.com/users/MockOfUser/gists{/gist_id}',
          "starred_url": 'https://api.github.com/users/MockOfUser/starred{/owner}{/repo}',
          "subscriptions_url": 'https://api.github.com/users/MockOfUser/subscriptions',
          "organizations_url": 'https://api.github.com/users/MockOfUser/orgs',
          "repos_url": 'https://api.github.com/users/MockOfUser/repos',
          "events_url": 'https://api.github.com/users/MockOfUser/events{/privacy}',
          "received_events_url": 'https://api.github.com/users/MockOfUser/received_events',
          "type": 'User',
          "site_admin": false
        },
        "parents": [

        ]
      }
    ].to_json
  end
end
