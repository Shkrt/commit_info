require 'json'
require 'hashie'

module GithubMockHelper
  def github_dummy_commits
    arr = []
    5.times{ arr << commit }
    arr
  end

  private

  def commit
    json_string = '{ "sha" :  "11124" ,
                     "commit" : {
                       "message" : "make tests pass",
                       "committer" : {
                         "name" : "qqqqq" ,
                         "email" : "12132@e3e.rt" ,
                         "date" :  "12.12.12"
                       }
                     }
                    }'
    hash = JSON.parse json_string
    Hashie::Mash.new hash
  end
end
