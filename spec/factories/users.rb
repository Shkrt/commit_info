FactoryGirl.define do
  factory :user do
    factory :user_with_commits do
      name 'Testuser'
      email 'test@te.st'
    end

    factory :user_without_commits do
      name 'Another'
      email 'pass@pa.ss'
    end
  end
end
