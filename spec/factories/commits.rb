FactoryGirl.define do
  sequence :date do |n|
    n.days.from_now
  end

  factory :commit do
    description 'update README'
    date 
    username 'Testuser'
    sha '1jl23jl123oijo2iobjgv1j'
  end
end
