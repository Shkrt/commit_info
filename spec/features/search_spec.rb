describe 'Searching feature' do
  context "when database is empty" do
    it 'is not available' do
      visit root_path
      expect(page).not_to have_css('#search_email')
    end
  end

  context "when database has records" do
    let!(:user) { create :user_with_commits }
    before do
      25.times{ create :commit }
    end

    it 'is available' do
      visit root_path
      expect(page).to have_css('#search_email')
    end

    it 'shows "nothing found" message if no matches found' do
      visit root_path
      fill_in('search_email', with: "does@not.exist" )
      find(:css, '#search_btn').click
      expect(page).to have_content(I18n.t("nothing_found"))
      expect(page).not_to have_css('#commits_table')
    end

    it 'shows commits table if matches are found' do
      visit root_path
      fill_in('search_email', with: user.email )
      find(:css, '#search_btn').click
      expect(page).not_to have_content(I18n.t("nothing_found"))
      expect(page).to have_css('#commits_table')
    end
  end
end
