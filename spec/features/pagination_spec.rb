describe 'Pagination' do
  context "when amount of commits is less than pagination constraint" do
    let!(:user) { create :user_with_commits }
    before do
      19.times{ create :commit }
    end

    it 'is not visible' do
      visit root_path
      expect(page).not_to have_css('.pagination')
    end
  end

  context "when amount of commits is more than pagination constraint" do
    let!(:user) { create :user_with_commits }
    before do
      25.times{ create :commit }
    end

    it 'is visible' do
      visit root_path
      expect(page).to have_css('.pagination')
    end

    it 'divides records according to pagination constraint' do
      visit root_path
      pages = page.all(:css, ".pagination>a").count
      expect(pages).to eq(2)
    end

    it 'shows only limited number of records on each page' do
      visit root_path
      records = page.all(:css, "tr[id^=commit_]").count
      expect(records).to eq(20)
    end
  end

end
