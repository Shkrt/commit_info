RSpec.describe CommitsController, type: :controller do
  before { 25.times { create :commit } }
  let!(:commit) { create :commit }
  describe 'GET#index' do
    it 'renders index template' do
      get :index, page: 2
      expect(response).to render_template(:index)
    end

    it 'renders index template for js request' do
      xhr :get, :index, page: 2
      expect(response).to render_template(:index)
    end
  end

  describe 'GET#search' do
    it 'renders search template' do
      get :search, search: { email: "email@em.l" }, page: 2
      expect(response).to render_template(:search)
    end
  end

  describe 'GET#edit' do
    it 'renders edit template' do
      xhr :get, :edit, id: commit.id, page: 2
      expect(response).to render_template(:edit)
    end
  end

  describe 'PATCH#update' do
    it 'renders index template' do
      xhr :patch, :update, commit: { username: "Test" , page: 1 }, id: commit.id
      expect(response).to render_template(:index)
    end

    it 'updates username for given commit' do
      xhr :patch, :update, commit: { username: "New_name" , page: "" }, id: commit.id
      expect(commit.reload.username).to eq("New_name")
    end
  end
end
