RSpec.describe GithubController, type: :controller do
  describe 'POST#fetch' do
    it 'renders index template' do
      post :fetch, repo_path: { user: "user", repo: "repo"}
      expect(response).to redirect_to root_path
      expect(flash[:notice]).to be_present
    end
  end
end
