include GithubMockHelper

describe "CreateRecords" do
  let(:commits) { github_dummy_commits }

  before(:each) do
    Commit.delete_all
    User.delete_all

    CreateRecords.perform(commits)
  end

  it ".perform creates instances of Commit objects" do
    expect(Commit.count).to eq 5
  end

  it ".perform creates instances of unique User objects" do
    expect(User.count).to eq 1
  end
end
