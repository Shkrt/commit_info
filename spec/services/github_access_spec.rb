include GithubStubHelper

describe "GithubAccess" do
  let(:response) { github_json }
  let(:parsed) { GithubAccess.new.get_commits("MockOfUser", "sktchpd") }

  before(:each) do
    stub_request(:get,
      "https://api.github.com/repos/MockOfUser/sktchpd/commits?access_token=#{ENV['GITHUB_ACCESS_TOKEN']}").
        to_return(body: response)
  end

  it ".get_commits returns an instance of Github::ResponseWrapper" do
    expect(parsed).to be_instance_of(Github::ResponseWrapper)
  end

  it ".get_commits returns a collection which size is equal to commits count" do
    expect(parsed.length).to eq 5
  end
end
