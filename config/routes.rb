Rails.application.routes.draw do
  root to: 'commits#index'
  post "fetch_commits" => "github#fetch"
  get "search" => "commits#search"
  resources :users, :commits
end
